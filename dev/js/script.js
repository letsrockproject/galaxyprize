$(document).ready(function() {
	$('.js-markered').on('focus', function(e) {
		var $el = $(e.target);
		var $currentMarker = $('.js-marker[data-id="' + $el.data('marker') + '"]');
		$currentMarker.siblings('.js-marker').fadeOut();
		$currentMarker.fadeIn();
		var scrollMarker = $currentMarker.position();
		var scrollClick = $el.position();
		var offset = scrollClick.top - scrollMarker.top;
		$('.js-bill').css('top', offset + 'px');
	});

	$('.js-markered').on('focusout', function() {
		$('.js-marker').fadeOut();
	});

	$(window).scroll(function() {
		$('.js-bill').css('top', 0);
	});

	$('.js-phone').on("keydown", function(e) {
		if ($('.js-phone').val().length === 0) {
			if (e.keyCode == 104) {
				$('.js-phone').mask("8(999) 999-99-99");
			} else {
				$('.js-phone').mask("+7(999) 999-99-99");
			}
		}
	});


	$('.modal').iziModal({
		width: 800,
		height: 571,
		'border-radius': 10
});

	const $form = $('.js-form');

	$.validator.addMethod('checkPhone', function(value, element) {
		return /\+\d{1} \(\d{3}\) \d{3} \d{2} \d{2}/g.test(value);
	});

	$.extend($.validator.messages, {
		checkPhone: 'Введите правильный номер телефона.',
		required: 'Это поле необходимо заполнить.',
		remote: 'Пожалуйста, введите правильное значение.',
		email: 'Пожалуйста, введите корректный email.',
		url: 'Пожалуйста, введите корректный URL.',
		date: 'Пожалуйста, введите корректную дату.',
		dateISO: 'Пожалуйста, введите корректную дату в формате ISO.',
		number: 'Пожалуйста, введите число.',
		digits: 'Пожалуйста, вводите только цифры.',
		creditcard: 'Пожалуйста, введите правильный номер кредитной карты.',
		equalTo: 'Пожалуйста, введите такое же значение ещё раз.',
		extension: 'Пожалуйста, выберите файл с расширением jpeg, pdf, doc, docx.',
		maxlength: $.validator.format(
				'Пожалуйста, введите не больше {0} символов.'),
		minlength: $.validator.format(
				'Пожалуйста, введите не меньше {0} символов.'),
		rangelength: $.validator.format(
				'Пожалуйста, введите значение длиной от {0} до {1} символов.'),
		range: $.validator.format('Пожалуйста, введите число от {0} до {1}.'),
		max: $.validator.format(
				'Пожалуйста, введите число, меньшее или равное {0}.'),
		min: $.validator.format(
				'Пожалуйста, введите число, большее или равное {0}.'),
		maxsize: 'Максимальный размер файла - 5мб',
	});

	$form.validate({
		errorPlacement: function(error, element) {
			return true;
		},
		success: function(element) {
			return true;
		},
		lang: 'ru',
		rules: {
			name: {
				required: true,
				minlength: 2,
			},
			email: {
				minlength: 2,
				email: true,
				required: true,
			},
			phone: {
				required: true,
				minlength: 17,
			},
			cost: {
				required: true,
			},
			date: {
				required: true,
				minlength: 10,
			},
			fn: {
				required: true,
				minlength: 16,
			},
			fp: {
				required: true,
				minlength: 10,
			},
			fd: {
				required: true,
				minlength: 5,
			},
			time: {
				required: true,
				minlength: 5,
			},
		},
		invalidHandler: function(form) {
			// const modal = $(".modal[data-modal='error']");
			// modal.iziModal('open');
		},
		submitHandler: function(form) {
			$form.trigger('reset');
			const modal = $('.modal[data-modal=\'success\']');
			modal.iziModal('open');
			// const $form = $(form);
			// let formData = $form.serializeArray();
			//
			// const fileData = $form.find('.js-file-input').prop('files')[0];
			//
			// formData = new FormData(form);

			$.ajax({
				url: form.action,
				type: 'post',
				data: formData,
				contentType: false,
				processData: false,
				dataType: 'text',
				cache: false,
				success: function(response) {
					$form.trigger('reset');
					const modal = $(".modal[data-modal='success']");
					modal.iziModal('open');
				},
			});
		},
	});

	$('.js-anchor').click(function() {
		var scroll_el = $(this).attr('href');
		if ($(scroll_el).length != 0) {
			$('html, body').animate({scrollTop: $(scroll_el).offset().top}, 500);
		}
		return false;
	});

});